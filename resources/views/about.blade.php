@extends('layout')

@section('title')
  Tentang 
@endsection

<body class="about">
<!-- Live Style Switcher Starts - demo only -->
<div id="switcher" class="">
    <div class="content-switcher">
        <h4>TAMPILAN</h4>
        <ul>
            <li>
                <a href="#" onclick="setActiveStyleSheet('purple');" title="purple" class="color"><img src="img/styleswitcher/purple.png" alt="purple"/></a>
            </li>
            <li>
                <a href="#" onclick="setActiveStyleSheet('red');" title="red" class="color"><img src="img/styleswitcher/red.png" alt="red"/></a>
            </li>
            <li>
                <a href="#" onclick="setActiveStyleSheet('blueviolet');" title="blueviolet" class="color"><img src="img/styleswitcher/blueviolet.png" alt="blueviolet"/></a>
            </li>
            <li>
                <a href="#" onclick="setActiveStyleSheet('blue');" title="blue" class="color"><img src="img/styleswitcher/blue.png" alt="blue"/></a>
            </li>
            <li>
                <a href="#" onclick="setActiveStyleSheet('goldenrod');" title="goldenrod" class="color"><img src="img/styleswitcher/goldenrod.png" alt="goldenrod"/></a>
            </li>
            <li>
                <a href="#" onclick="setActiveStyleSheet('magenta');" title="magenta" class="color"><img src="img/styleswitcher/magenta.png" alt="magenta"/></a>
            </li>
            <li>
                <a href="#" onclick="setActiveStyleSheet('yellowgreen');" title="yellowgreen" class="color"><img src="img/styleswitcher/yellowgreen.png" alt="yellowgreen"/></a>
            </li>
            <li>
                <a href="#" onclick="setActiveStyleSheet('orange');" title="orange" class="color"><img src="img/styleswitcher/orange.png" alt="orange"/></a>
            </li>
            <li>
                <a href="#" onclick="setActiveStyleSheet('green');" title="green" class="color"><img src="img/styleswitcher/green.png" alt="green"/></a>
            </li>
            <li>
                <a href="#" onclick="setActiveStyleSheet('yellow');" title="yellow" class="color"><img src="img/styleswitcher/yellow.png" alt="yellow"/></a>
            </li>
        </ul>

        <a href="https://themeforest.net/item/tunis-personal-portfolio/26761598?irgwc=1&amp;clickid=UBs0rDX6YxyJWfewUx0Mo3E1UkiwozXD10XATw0&amp;iradid=275988&amp;irpid=1327395&amp;iradtype=ONLINE_TRACKING_LINK&amp;irmptype=mediapartner&amp;mp_value1=&amp;utm_campaign=af_impact_radius_1327395&amp;utm_medium=affiliate&amp;utm_source=impact_radius" class="waves-effect waves-light font-weight-700 purchase"><i class="fa fa-shopping-cart"></i> Purchase</a>
        <div id="hideSwitcher">&times;</div>
    </div>
</div>
<div id="showSwitcher" class="styleSecondColor"><i class="fa fa-cog fa-spin"></i></div>
<!-- Live Style Switcher Ends - demo only -->
<!-- Header Starts -->
<header class="header" id="navbar-collapse-toggle">
    <!-- Fixed Navigation Starts -->
    <ul class="icon-menu d-none d-lg-block revealator-slideup revealator-once revealator-delay1">
        <li class="icon-box">
            <i class="fa fa-home"></i>
            <a href="/">
                <h2>Home</h2>
            </a>
        </li>
        <li class="icon-box active">
            <i class="fa fa-envelope-open"></i>
            <a href="kontak">
                <h2>Contact</h2>
            </a>
        </li>
    </ul>
    <!-- Fixed Navigation Ends -->
    <!-- Mobile Menu Starts -->
    <nav role="navigation" class="d-block d-lg-none">
        <div id="menuToggle">
            <input type="checkbox" />
            <span></span>
            <span></span>
            <span></span>
            <ul class="list-unstyled" id="menu">
                <li><a href="index.html"><i class="fa fa-home"></i><span>Home</span></a></li>
                <li class="active"><a href="about.html"><i class="fa fa-user"></i><span>About</span></a></li>
            </ul>
        </div>
    </nav>
    <!-- Mobile Menu Ends -->
</header>
<!-- Header Ends -->
<!-- Page Title Starts -->
<section class="title-section text-left text-sm-center revealator-slideup revealator-once revealator-delay1">
    <h1>Tentang <span>saya</span></h1>
    <span class="title-bg">About Me</span>
</section>
<!-- Page Title Ends -->
<!-- Main Content Starts -->
<section class="main-content revealator-slideup revealator-once revealator-delay1">
    <div class="container">
        <div class="row">
            <!-- Personal Info Starts -->
            <div class="col-12 col-lg-5 col-xl-6">
                <div class="row">
                    <div class="col-12">
                        <h3 class="text-uppercase custom-title mb-0 ft-wt-600">info personal</h3>
                    </div>
                    <div class="col-12 d-block d-sm-none">
                        <img src="img/edii.jpg" class="img-fluid main-edii" alt="my picture" />
                    </div>
                    <div class="col-6">
                        <ul class="about-list list-unstyled open-sans-font">
                           
                            <li> <span class="title">Nama :</span> <span class="value d-block d-sm-inline-block d-lg-block d-xl-inline-block">Kadek Darmawan</span> </li>
                            <li> <span class="title">Umur :</span> <span class="value d-block d-sm-inline-block d-lg-block d-xl-inline-block">20 Tahun</span> </li>
                            <li> <span class="title">Tempat Lahir :</span> <span class="value d-block d-sm-inline-block d-lg-block d-xl-inline-block">Bali</span> </li>
                            <li> <span class="title">Tanggal Lahir :</span> <span class="value d-block d-sm-inline-block d-lg-block d-xl-inline-block">22 November 2000</span> </li>
                        </ul>
                    </div>
                    <div class="col-6">
                        <ul class="about-list list-unstyled open-sans-font">
                            <li> <span class="title">Alamat :</span> <span class="value d-block d-sm-inline-block d-lg-block d-xl-inline-block">Penyabangan, Gerokgak, Buleleng, Bali</span> </li>
                            <li> <span class="title">Nomor Hp :</span> <span class="value d-block d-sm-inline-block d-lg-block d-xl-inline-block">085238070394</span> </li>
                            <li> <span class="title">Email :</span> <span class="value d-block d-sm-inline-block d-lg-block d-xl-inline-block">kadekdermawan22@gmail.com</span> </li>
                          
                        </ul>
                    </div>
                    <div class="col-12 mt-3">
                        <a href="about" class="btn btn-download">Download CV</a>
                    </div>
                </div>
            </div>
            <!-- Personal Info Ends -->
            <!-- Boxes Starts -->
            <div class="col-12 col-lg-7 col-xl-6 mt-5 mt-lg-0">
                <div class="row">
                <div class="col-6">
                        <div class="box-stats with-margin">
                            <h3 class="poppins-font position-relative">motto</h3>
                            <p class="open-sans-font m-0 position-relative text-uppercase">Berpikir sukses Jika Ingin Sukses<span class="d-block">Berpikirlah Bahagia Jika Ingin Bahagia</span></p>
                        </div>
                    </div>
                    <div class="col-6">
                        <div class="box-stats with-margin">
                            <h3 class="poppins-font position-relative">hobi</h3>
                            <p class="open-sans-font m-0 position-relative text-uppercase">Game<span class="d-block">Biliard</span></p>
                        </div>
                    </div>
                    <div class="col-6">
                        <div class="box-stats">
                            <h3 class="poppins-font position-relative">status</h3>
                            <p class="open-sans-font m-0 position-relative text-uppercase">Mahasiswa<span class="d-block">atau Pelajar</span></p>
                        </div>
                    </div>
                    <div class="col-6">
                        <div class="box-stats">
                            <h3 class="poppins-font position-relative">cita2</h3>
                            <p class="open-sans-font m-0 position-relative text-uppercase">jadi <span class="d-block">Programer</span></p>
                        </div>
                    </div>
                </div>
            </div>
            <!-- Boxes Ends -->
        </div>
      
        <hr class="separator mt-1">
        <!-- Experience & Education Starts -->
        <div class="row">
            <div class="col-12">
                <h3 class="text-uppercase pb-5 mb-0 text-left text-sm-center custom-title ft-wt-600">Pengalaman Organisasi <span>&</span> Pendidikan</h3>
            </div>
            <div class="col-lg-6 m-15px-tb">
                <div class="resume-box">
                    <ul>
                        <li>
                            <div class="icon">
                                <i class="fa fa-briefcase"></i>
                            </div>
                            <span class="time open-sans-font text-uppercase">2020 - Sekarang </span>
                            <h5 class="poppins-font text-uppercase">BadaN Eksekutif Mahasiswa<span class="place open-sans-font">Undiksha, Singaraja, Bali</span></h5>
                            <p class="open-sans-font">Saya Sebagai Staff Pelayanan Publik di Organisasi Badan Eksekutif Mahasiswa Undiksha</p>
                        </li>
                        <li>
                            <div class="icon">
                                <i class="fa fa-briefcase"></i>
                            </div>
                            <span class="time open-sans-font text-uppercase">2013 - 2016</span>
                            <h5 class="poppins-font text-uppercase">OSIS <span class="place open-sans-font">SMP N 4 Gerokgak  </span></h5>
                            <p class="open-sans-font">Saya Bergabung sebagai Bendahara (Osis Inti) Organisasi ditingkat Menengah pertama SMP N 4 Gerokgak</p>
                        </li>
                        <li>
                            <div class="icon">
                                <i class="fa fa-briefcase"></i>
                            </div>
                            <span class="time open-sans-font text-uppercase">2017-2018</span>
                            <h5 class="poppins-font text-uppercase">Pramuka <span class="place open-sans-font">SMA N 1 Belitang III</span></h5>
                            <p class="open-sans-font">Bergabung dengan beberapa organisasi yang ada ditingkat menengah atas</p>
                        </li>
                        <li>
                            <div class="icon">
                                <i class="fa fa-briefcase"></i>
                            </div>
                            <span class="time open-sans-font text-uppercase">2019</span>
                            <h5 class="poppins-font text-uppercase">PAT<span class="place open-sans-font">UNIVERSITAS PENDIDIKAN GANESHA</span></h5>
                            <p class="open-sans-font">mengikuti kepanitiaan kegiatan Pergelaran Akhir Tahun yang diadakan fakultas di UNIVERSITAS PENDIDIKAN GANESHA</p>
                        </li>
                    </ul>
                </div>
            </div>
            <div class="col-lg-6 m-15px-tb">
                <div class="resume-box">
                    <ul>
                        <li>
                            <div class="icon">
                                <i class="fa fa-graduation-cap"></i>
                            </div>
                            <span class="time open-sans-font text-uppercase">2013</span>
                            <h5 class="poppins-font text-uppercase">lulus tingkat sekolah dasar <span class="place open-sans-font">SD N 1 Penyabangan</span></h5>
                            <p class="open-sans-font">Saya menempuh sekolah dasar tepatnya di Desa Penyabangan selama 6 tahun</p>
                        </li>
                        <li>
                            <div class="icon">
                                <i class="fa fa-graduation-cap"></i>
                            </div>
                            <span class="time open-sans-font text-uppercase">2016</span>
                            <h5 class="poppins-font text-uppercase">lulus tingkat sekolah menengah pertama<span class="place open-sans-font">SMP N 4 Gerokgak</span></h5>
                            <p class="open-sans-font">Saya menempuh sekolah menengah pertama tepatnya di Desa Penyabangan selama 3 tahun</p>
                        </li>
                        <li>
                            <div class="icon">
                                <i class="fa fa-graduation-cap"></i>
                            </div>
                            <span class="time open-sans-font text-uppercase">2019</span>
                            <h5 class="poppins-font text-uppercase">lulus tingkat sekolah menengah atas<span class="place open-sans-font">SMA N 1 Gerokgak</span></h5>
                            <p class="open-sans-font">Saya menempuh sekolah menengah atas tepatnya di Desa Penyabangan selama 3 tahun</p>
                        </li>
                        <li>
                            <div class="icon">
                                <i class="fa fa-graduation-cap"></i>
                            </div>
                            <span class="time open-sans-font text-uppercase">2019</span>
                            <h5 class="poppins-font text-uppercase">sedang menempuh jenjang perkuliahan<span class="place open-sans-font">UNIVERSITAS PENDIDIKAN GANESHA</span></h5>
                            <p class="open-sans-font">Saat ini saya sedang menempuh perkuliahan di UNIVERSITAS PENDIDIKAN GANESHA, di Bali</p>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
        <!-- Experience & Education Ends -->
    </div>
</section>
<!-- Main Content Ends -->

<!-- Template JS Files -->
<script src="js/jquery-3.5.0.min.js"></script>
<script src="js/styleswitcher.js"></script>
<script src="js/preloader.min.js"></script>
<script src="js/fm.revealator.jquery.min.js"></script>
<script src="js/imagesloaded.pkgd.min.js"></script>
<script src="js/masonry.pkgd.min.js"></script>
<script src="js/classie.js"></script>
<script src="js/cbpGridGallery.js"></script>
<script src="js/jquery.hoverdir.js"></script>
<script src="js/popper.min.js"></script>
<script src="js/bootstrap.js"></script>
<script src="js/custom.js"></script>

</body>


<!-- Mirrored from slimhamdi.net/tunis/dark/about.html by HTTrack Website Copier/3.x [XR&CO'2014], Wed, 24 Mar 2021 11:59:27 GMT -->
</html>
