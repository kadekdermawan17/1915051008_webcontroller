<?php
// use App\Models\index;
// use App\Models\about;
// use App\Models\kontak;


Route::get('/', [App\Http\Controllers\IndexController::class, 'index']);
Route::get('/about', [App\Http\Controllers\AboutController::class, 'about']);
Route::get('/kontak',[App\Http\Controllers\kontakController::class, 'kontak']);
Route::get('/portfolio',[App\Http\Controllers\PortfolioController::class, 'portfolio']);
Route::get('/layout',[App\Http\Controllers\LayoutController::class, 'layout']);